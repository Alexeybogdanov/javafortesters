package de.test;

        import java.util.ArrayList; //why spaces before import? usually package, import and public class on one level
        import java.util.Arrays; // use ctrl + alt + o to optimize imports
        import java.util.List;
        import java.util.Random;

/**
 * Write here short purpose of this class.
 * Also can be useful File -> Settings -> File and code templates.
 *
 * Created by albogdan on 18.10.2017.
 */
public class MyArray {

    /*
    private static final int COLUMN_CONST =5; it's constant - UPPER_SNAKE_CASE
     */

    Random rnd = new Random();// no reason to create it each time in the method, also with Random a lot of creating can be performance issue
    private int COLUMN; //java coders want to see constants in UPPER_SNAKE_CASE and class fields in camelCase :)
    private int ROW;// so, it's field and better name it in camelCase
    private byte[][] arr;

    public byte[][] getArr() {
        return arr;
    }

    public MyArray(int COLUMN, int ROW) {
        this.COLUMN = COLUMN;
        this.ROW = ROW;
        this.arr = new byte[COLUMN][ROW];

    }

    public void fill() {
        List<Byte> col0 = new ArrayList<>();//it's better to say that you want to store in this list not something from the Object
        //but actually objects of class Integer or Byte, i.e. List<Integer> , List<Byte>. it's calls generic we will see it later

        for (int i = 0; i < arr.length; i++) {
            byte x;//try to define variables very near to their use
            do {
                x = (byte) rnd.nextInt(10);
            } while (col0.contains(x));

            col0.add(x);
            arr[i][0] = x; //fill array

            List<Byte> row = new ArrayList<>();
            for (int j = 0; j < arr.length - 1; j++) {
                byte y;
                row.add(x); //allow to remove 1 condition in while
                do {
                    y = (byte) rnd.nextInt(10);
                } while (row.contains(y) || checkColumnHasValue(i, j, y));

                row.add(y);

                arr[i][j + 1] = y; //fill array
            }
        }
        //System.out.println(Arrays.deepToString(arr));
    }

    /**
     * Print arrays to the console.
     */
    public void showArray() {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    //methods used only inside the class should be declare with private access to leave class API to other classes clear
    private boolean checkColumnHasValue(int col, int row, int randValue) {
        for (int i = col; i > 0; i--) {
            if (arr[i - 1][row + 1] == randValue) {
                return true;
            }
        }
        return false;
    }

}