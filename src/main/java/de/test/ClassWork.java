package de.test;

import java.util.Arrays;

/**
 * Created by albogdan on 20.10.2017.
 */
public class ClassWork {
    public static void start() {
        //Task1
        int a = 4;
        int b = 6;
        int c = 11;
        task1(a, b, c);

        //Task3
        int[] sal = {36, 11, 20};
        System.out.println("разница = " + task3(sal));

        //Task2
        int[][] arr = {
                {26, 17},
                {13, 15},
                {19, 11},
                {14, 16}
        };
        task2(arr);


    }

    private static void task1(int a, int b, int c) {
        if (a + b < c && a * b < c) {
            System.out.println(true);
        } else System.out.println(false);
    }

    private static int task3(int[] sal) {
        Arrays.sort(sal);
        int res = sal[sal.length - 1] - sal[0];

        return res;
    }
    private static void task2(int[][] arr){
        int team1 = 0;
        int team2 = 0;
        for (int i = 0; i < arr.length ; i++) {
            team1 += arr[i][0];
            team2 += arr[i][1];

        }
        if (team1 > team2){
            System.out.println("1");
        } else if (team1 < team2){
            System.out.println("2");
        }else {
            System.out.println("DRAW");
        }
    }
}
