package de.test;

import de.cripto.PasswordChecker;

import java.io.IOException;

/**
 * Created by albogdan on 19.10.2017.
 */
public class Main {
    public static void main(String[] args) {
        /*MyArray myArr = new MyArray(5,5);
        myArr.fill();
        myArr.showArray();*/

        PasswordChecker passwordChecker = new PasswordChecker();
        try {
            System.out.println(
                    passwordChecker.isPasswordStrong(passwordChecker.obtainPassword()) ? "pass is strong" : "pass is weak"
            );
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}
