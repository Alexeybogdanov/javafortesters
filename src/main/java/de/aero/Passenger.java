package de.aero;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Passenger {
    private String fullName;
    private String birdthDate;     //dd/MM/YYYY
    private int passNumber;

    public Passenger(String fullName, String birdthDate, int passNumber) throws Exception {
        if (checkPassengerAge(birdthDate)) {
            this.fullName = fullName;
            this.birdthDate = birdthDate;
            this.passNumber = passNumber;
        } else throw new Exception("passengers age should be greater then 1 year");
    }


    public String getFullName() {
        return fullName;
    }

    public int getPassNumber() {
        return passNumber;
    }
    private boolean checkPassengerAge(String birdthDate) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        long currentDate = new Date().getTime();
        long checkDate = (formatter.parse(birdthDate)).getTime();
        return ((currentDate - checkDate) / 1000 > (365 * 24 * 3600)); // делю на 1000, иначе не лезло в long, а c bigInteger громоздко было
                                                                       //на високосный год забиваем)
    }

    @Override
    public String toString() {
        return fullName;
    }
}
