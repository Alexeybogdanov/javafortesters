package de.aero;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Flight {
    private int flightNumber;
    private List<Passenger> passengerList;
    private Map<Seat, Passenger> seatList;

    public Flight(int flightNumber) {
        this.flightNumber = flightNumber;

    }

    public List<Passenger> getPassengerList() {
        return passengerList;
    }


    public void addPassenger(Passenger passenger) {
        passengerList = new ArrayList<>();
        passengerList.add(passenger);

    }

    public void registerPassenger(Passenger passenger, Seat seat) {
        seatList = new HashMap<>();
        if (getPassengerList().contains(passenger)) {
            seatList.put(seat, passenger);
            seat.setPassenger(passenger);
        }
    }

    public void showAllSeats() {

        seatList.forEach((s, p) -> System.out.println("Seat : " + s + " Passenger : " + p));
    }
}
