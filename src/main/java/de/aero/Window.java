package de.aero;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame {
    public Window() {
        setTitle("FlyAway Registration Form"); //заголовок окна
        setBounds(300, 600, 400, 400); // точка 0.0 вверху слева.
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);  //завершить программу при закрытии окна
        setLocationRelativeTo(null);   //поместить в центр экрана


        setLayout(new GridLayout(1,2));

        JPanel jpLeft = new JPanel();
        add(jpLeft);
        JPanel jpRight = new JPanel();
        add(jpRight);
        jpLeft.setBackground(Color.GRAY);

        //left panel
        jpLeft.setLayout(new BoxLayout(jpLeft,BoxLayout.Y_AXIS));

        JLabel nameLabel = new JLabel("Full Name:");
        JLabel birthLabel = new JLabel("Birthdate: in format DD/MM/YYYY");
        JLabel passportLabel = new JLabel("Passport number:");

        JTextField nameField = new JTextField();   //однострочный тектстовой ввод
        JTextField birthdateField = new JTextField();   //однострочный тектстовой ввод
        JTextField passportField = new JTextField();   //однострочный тектстовой ввод

//        jpLeft.add(textField, BorderLayout.CENTER);
        jpLeft.add(nameLabel);
        jpLeft.add(nameField);

        jpLeft.add(birthLabel);
        jpLeft.add(birthdateField);

        jpLeft.add(passportLabel);
        jpLeft.add(passportField);

        //        textField.setBackground(Color.white);





        //right panel
        jpRight.setLayout(new BoxLayout(jpRight,BoxLayout.Y_AXIS));
        JTextArea textAreaPassenger = new JTextArea();
        JScrollPane jsp = new JScrollPane(textAreaPassenger);
        textAreaPassenger.setEditable(false);
        jpRight.add(jsp);


        //кнопка
        JButton jAdd = new JButton("Add Passenger");
        jAdd.setBackground(Color.LIGHT_GRAY);
//        jpLeft.add(jSend, BorderLayout.EAST);
        jpLeft.add(jAdd);
        jAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Passenger passenger = null;
                int intPass = Integer.parseInt(passportField.getText());
                try {
                    passenger = new Passenger(nameField.getText(),birthdateField.getText(),intPass);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                textAreaPassenger.append(passenger.getFullName()); // отправить в textArea то что получиено из textField
                nameField.setText(""); //очистить textField
                birthdateField.setText(""); //очистить textField
                passportField.setText(""); //очистить textField
                //textField.grabFocus();
            }
        });



//        textArea.setLineWrap(true);  //тект переноситься на следущую строку
//        textArea.setWrapStyleWord(true); //текст переносится без разрыва слов
        //jta.setPreferredSize(new Dimension(100,100));
//
//        JScrollPane jsp = new JScrollPane(textArea); //обернули его скролом
//        jpUp.add(jsp, BorderLayout.CENTER); //добавили получившееся на верхнюю панель
//        add(jpUp, BorderLayout.CENTER);   //поместили всю верхнюю панель вверх основного окна

        //нижняя панель
       // JPanel jpDown = new JPanel(new BorderLayout());



//        textField.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                textArea.append(textField.getText() + "\n"); // отправить в textArea то что получиено из textField
//                textField.setText(""); //очистить textAreaA
//                textField.grabFocus();
//            }
//        });

        //кнопка
//        JButton jSend = new JButton("Send");
//        jSend.setBackground(Color.gray);
//        jpDown.add(jSend, BorderLayout.EAST);
//        jSend.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                textArea.append(textField.getText() + "\n"); // отправить в textArea то что получиено из textField
//                textField.setText(""); //очистить textField
//                textField.grabFocus();
//            }
//        });
//
//        add(jpDown, BorderLayout.SOUTH);  //поместили всю нижнюю панель снизу основного окна
        setVisible(true);
    }
}