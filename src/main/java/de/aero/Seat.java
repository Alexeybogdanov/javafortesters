package de.aero;

import org.apache.commons.lang3.EnumUtils;

public class Seat {
    private int rowNumber;
    private Chair chair;
    private Passenger passenger;

    public Seat(int rowNumber, Chair chair) {
        if (rowNumber <= 5 & EnumUtils.isValidEnum(Chair.class, chair.toString())) {  //наверно енумы дополнительно проверять не надо, но оставлю на память
            this.rowNumber = rowNumber;
            this.chair = chair;
        } else throw new RuntimeException("seat value or range is invalid");
    }

    public Chair getChair() {
        return chair;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    @Override
    public String toString() {
        return "rowNumber=" + rowNumber +
                ", chair=" + chair;
    }
}
