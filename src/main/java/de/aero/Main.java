package de.aero;

public class Main {

    public static void main(String[] args) throws Exception {
        Window window = new Window();

        Passenger alex = new Passenger("alex bogdanov", "29/06/1982", 123456);
        System.out.println("Создали пассажира: alex \n");

        Seat s1 = new Seat(5,Chair.A);
        System.out.println("создали пустое место " +s1.getChair() +" ряд " + s1.getRowNumber()  +"\n");

        Flight flight = new Flight(12345);
        System.out.println("создали рейс: 12345 \n");

        flight.addPassenger(alex);
        System.out.println("добавили пассажира в список пассажиров на рейс 12345 \n");

        flight.registerPassenger(alex,s1);
        System.out.println("зарегестрировали alex на рейс \n");

        System.out.println("распечатали список мест");
        flight.showAllSeats();

    }





}
