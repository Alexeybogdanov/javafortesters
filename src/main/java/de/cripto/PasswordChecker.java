package de.cripto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by albogdan on 25.10.2017.
 * password validator
 */
public class PasswordChecker {

    private static final String PATTERN = "((?=.*\\d)" +
            "(?=.*[a-z])" +
            "(?=.*[A-Z])" +
            "(?!.*[!@#$%&*])" +
            ".{8,15})";

    private BufferedReader reader;

    public PasswordChecker() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    public boolean isPasswordStrong(String passwordToCheck) {
        return passwordToCheck.matches(PATTERN);
    }

    public String obtainPassword() throws IOException {
        System.out.print("Use [a-z] & [A-Z] and don't use [!@#$%&*], the rest  is allowed... \nCheck your password : \t");
        return reader.readLine();
    }

}