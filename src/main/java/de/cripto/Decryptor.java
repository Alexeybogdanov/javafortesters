package de.cripto;

/**
 * Created by albogdan on 25.10.2017.
 */
public class Decryptor {

    public String decode(String encodedPassword) {
        //String s = "shacnidw#";

        //create array from input word
        char[] word = encodedPassword.toCharArray();

        //calculate edges
        int part1Length;
        if (word.length % 2 == 0) {
            part1Length = word.length / 2;
        } else {
            part1Length = word.length / 2 + 1;
        }
        int part2Length = word.length / 2;

        //let's append it to the StringBuilder immediately without temporary arrays
        StringBuilder str1 = new StringBuilder();
        for (int i = 0, j = 0; j < part1Length; i += 2, j++) { //great cycle!
            if (encodedPassword.charAt(i) != '#') {
                str1.append(encodedPassword.charAt(i));
            }
        }

        StringBuilder str2 = new StringBuilder();
        for (int i = 1, j = 0; j < part2Length; i += 2, j++) {
            if (encodedPassword.charAt(i) != '#') {
                str2.append(encodedPassword.charAt(i));
            }
        }

        return str1.append(str2.reverse()).toString();
    }

}
