package de.task6;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.util.Collections;
import java.util.List;

public class Contract implements IContractService, Comparable<Contract> {

    private final int contractNumber;
    /**
     * Total monthly price for tariff and all options.
     */
    private int totalMonthlyPrice;
    /**
     * Date until contract is valid.
     */
    private LocalDate validTo;

    public Contract(int contractNumber, LocalDate validTo) {
        this.contractNumber = contractNumber;
        this.validTo = validTo;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }

    public int getTotalMonthlyPrice() {
        return totalMonthlyPrice;
    }

    public void setTotalMonthlyPrice(int totalMonthlyPrice) {
        this.totalMonthlyPrice = totalMonthlyPrice;
    }

    public LocalDate getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }

    @Override
    public List<Contract> sortContractsByTotalMonthlyPrice(List<Contract> incomeContractsList) {
        Collections.sort(incomeContractsList);
        return incomeContractsList;
    }

    @Override
    public boolean isContractsEqual(Contract contract1, Contract contract2) {
        return contract1.equals(contract2);
    }

    @Override
    public long calculateDaysUntilContractExpire(Contract contract) {
        LocalDate currentDate = LocalDate.now();
        LocalDate expiratioDate = contract.getValidTo();
        return Duration.between(currentDate.atStartOfDay(), expiratioDate.atStartOfDay()).toDays();
    }

    @Override
    public int compareTo(Contract another) {
        return this.getTotalMonthlyPrice() - another.getTotalMonthlyPrice();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } if (this == obj){
            return true;
        } if (obj instanceof Contract){
            Contract another  = (Contract) obj;
            if ((int)getContractNumber() == (int)another.getContractNumber())
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
       Integer i = getTotalMonthlyPrice();
       return Integer.toString(i);
    }
}
