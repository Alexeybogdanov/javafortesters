package de.task6;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by albogdan on 27.11.2017.
 */
public class Main {
    public static void main(String[] args) {
        Contract contract1 = new Contract(12345678, LocalDate.parse("2018-01-01") );
        Contract contract2 = new Contract(23456789, LocalDate.parse("2019-01-16") );
        Contract contract3 = new Contract(34567891, LocalDate.parse("2020-01-16") );
        Contract contract4 = new Contract(34567891, LocalDate.parse("2021-01-16") );
        contract1.setTotalMonthlyPrice(90);
        contract2.setTotalMonthlyPrice(12);
        contract3.setTotalMonthlyPrice(14);
        contract4.setTotalMonthlyPrice(100);

        List<Contract> contractsList = new ArrayList<>();
        contractsList.add(contract1);
        contractsList.add(contract2);
        contractsList.add(contract3);
        contractsList.add(contract4);

        System.out.println("not sorted list :" +contractsList);

        contract1.sortContractsByTotalMonthlyPrice(contractsList);
        System.out.println("sorted list by price :" +contractsList);

        System.out.println("============================================== + \n");

        System.out.println("comparing contract3 and contract4 :" +contract1.isContractsEqual(contract3,contract4));


        System.out.println("days between expiration date : " +contract1.calculateDaysUntilContractExpire(contract1));
    }
}
