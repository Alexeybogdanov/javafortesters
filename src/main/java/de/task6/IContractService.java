package de.task6;

import java.util.List;

public interface
IContractService {
    /**
     * Takes some contracts list and return new contracts list sorted by
     * total monthly price ascending.
     *
     * @param incomeContractsList
     * @return
     */
    List<Contract> sortContractsByTotalMonthlyPrice(List<Contract> incomeContractsList);

    /**
     * Takes two contracts and return <code>true</code>
     * if their
     * contractNumber equal, otherwise <code>false</code>
     * .
     *
     * @param contract1
     * @param contract2
     * @return
     */
    boolean isContractsEqual(Contract contract1, Contract contract2);

    /**
     * Return amount of days between contract validTo date and current date.
     * Could be returned negative value if contract is already expired.
     *
     * @param contract
     * @return
     */
    long calculateDaysUntilContractExpire(Contract contract);
}