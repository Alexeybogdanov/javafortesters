package de.task5;

/**
 * Created by albogdan on 13.11.2017.
 */
public interface Nose {
    public int iMethod();
}

abstract class Picasso implements Nose {
    @Override
    public int iMethod() {
        return 7;
    }
}

class Clowns extends Picasso {

}

class Acts extends Picasso {
    @Override
    public int iMethod() {
        return 5;
    }
}

