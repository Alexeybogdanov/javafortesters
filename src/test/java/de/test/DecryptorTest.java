package de.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.cripto.Decryptor;

/**
 * Test for <code>Decryptor</code>.
 */
public class DecryptorTest {

    private Decryptor decryptor;

    @Before
    public void setUp() {
        decryptor = new Decryptor();
    }

    @Test
    public void test1() {
        String originalString = "sandwich";
        String decodedString = decryptor.decode("shacnidw#");
        Assert.assertTrue(originalString.equals(decodedString));
    }

    @Test
    public void test2() {
        String originalString = "construction";
        String decodedString = decryptor.decode("cnoonisttcru#");
        Assert.assertTrue(originalString.equals(decodedString));
    }

    @After
    public void after() {
        decryptor = null;
    }

}
