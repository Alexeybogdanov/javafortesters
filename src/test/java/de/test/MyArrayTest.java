package de.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by albogdan on 19.10.2017.
 */
public class MyArrayTest {
    MyArray array;

    @Before
    public void init() {
        array = new MyArray(5, 5);
        array.fill();
    }

    //TODO normal test
    @Test
    public void simpleTest() {
        List testList = new ArrayList();
        byte[][] testArray = array.getArr();

        for (int i = 1; i < testArray.length; i++) { //collect first row without [0][0]
            testList.add(testArray[0][i]);

        }
        for (int j = 1; j < testArray.length; j++) { //collect first column  without[0][0]
            testList.add(testArray[j][0]);
        }
        Assert.assertFalse("Error: value " + testArray[0][0] + " is not unic !", testList.contains(testArray[0][0]));
    }
}