package de.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.cripto.PasswordChecker;

/**
 * Test for <code>PasswordChecker</code>.
 */
public class PasswordCheckerTest {

    private PasswordChecker passwordChecker;

    @Before
    public void setUp() {
        passwordChecker = new PasswordChecker();
    }

    @Test
    public void testPositive() {
       Assert.assertTrue(passwordChecker.isPasswordStrong("Fuh3)Fuh"));
    }

    @Test
    public void testNegative() {
        Assert.assertFalse(passwordChecker.isPasswordStrong("FuhFuhFuh"));
    }

    @After
    public void after() {
        passwordChecker = null;
    }

}
